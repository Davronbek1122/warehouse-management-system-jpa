package uz.pdp.b21jpapracticesimple.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.procedure.spi.ParameterRegistrationImplementor;
import uz.pdp.b21jpapracticesimple.enums.CustomFilterType;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqProductFilter {
    private boolean priceDescending;
    private CustomFilterType filterType;
    private String name;

}
