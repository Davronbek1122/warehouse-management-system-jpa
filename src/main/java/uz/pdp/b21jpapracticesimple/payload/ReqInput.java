package uz.pdp.b21jpapracticesimple.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class ReqInput {
    @NotNull
    private Integer warehouse_id;

    @NotNull
    private List<ReqInputProduct> reqInputProductList;
}
