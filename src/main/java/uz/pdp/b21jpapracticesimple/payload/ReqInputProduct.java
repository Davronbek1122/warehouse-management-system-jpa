package uz.pdp.b21jpapracticesimple.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqInputProduct {
    private Integer product_id;
    private Integer amount;
}
