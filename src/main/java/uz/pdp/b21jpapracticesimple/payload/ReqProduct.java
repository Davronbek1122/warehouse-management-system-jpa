package uz.pdp.b21jpapracticesimple.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqProduct {
    private Integer id;

    @NotNull(message = "Name is mandatory")
    private String name;

    @NotNull(message = "Price is mandatory")
    private double price;

    public ReqProduct(String name, double price) {
        this.name = name;
        this.price = price;
    }
}
