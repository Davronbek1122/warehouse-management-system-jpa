package uz.pdp.b21jpapracticesimple.enums;

public enum CustomFilterType {
    STARTS_WITH,
    CONTAINS,
    ENDS_WITH
}
