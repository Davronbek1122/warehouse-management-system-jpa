package uz.pdp.b21jpapracticesimple.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.b21jpapracticesimple.entity.Warehouse;
import uz.pdp.b21jpapracticesimple.payload.ApiRes;
import uz.pdp.b21jpapracticesimple.service.WarehouseService;

@RestController
@RequestMapping("api/warehouse")
public class WarehouseController {
    @Autowired
    WarehouseService warehouseService;

    @PostMapping("/save")
    public HttpEntity<?> save (@RequestBody Warehouse warehouse){
        ApiRes res = warehouseService.save(warehouse);
        return ResponseEntity.status(res.isSuccess()?201:409).body(res);
    }

    @GetMapping("/all")
    public ApiRes getAll(){
        return warehouseService.getAll();
    }

    @GetMapping("/byId/{id}")
    public ApiRes getByWarehouseId(@PathVariable Integer id){
        return warehouseService.getById(id);
    }


}
