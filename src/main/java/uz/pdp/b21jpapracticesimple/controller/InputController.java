package uz.pdp.b21jpapracticesimple.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.b21jpapracticesimple.entity.Input;
import uz.pdp.b21jpapracticesimple.payload.ApiRes;
import uz.pdp.b21jpapracticesimple.payload.ReqInput;
import uz.pdp.b21jpapracticesimple.service.InputService;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/input")
public class InputController {
    @Autowired
    InputService inputService;

    @PostMapping("/save")
    public ApiRes save (@Valid @RequestBody ReqInput reqInput){
        return inputService.save(reqInput);
    }

}
