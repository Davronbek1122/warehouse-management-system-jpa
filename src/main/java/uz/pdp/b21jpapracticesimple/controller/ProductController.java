package uz.pdp.b21jpapracticesimple.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.b21jpapracticesimple.entity.Product;
import uz.pdp.b21jpapracticesimple.payload.ApiRes;
import uz.pdp.b21jpapracticesimple.payload.ReqProductFilter;
import uz.pdp.b21jpapracticesimple.service.ProductService;

import javax.validation.Valid;


@RestController
@RequestMapping("/api/product")
public class ProductController {
    @Autowired
    ProductService productService;

    @PostMapping("/save")
    public ApiRes save(@RequestBody @Valid Product product){
        return productService.save(product);
    }
    @PostMapping("/edit/{id}")
    public ApiRes edit(@PathVariable Integer id,@RequestBody @Valid Product product){
        if (id != null){
            ApiRes byId = productService.getById(id);
            Product productOld = (Product) byId.getObject();
            productOld.setName(product.getName());
            productOld.setPrice(product.getPrice());
            productService.save(productOld);
            return new ApiRes("successfully edited",true);
        }
        return new ApiRes("ProductId can not be null",false);
    }

    @DeleteMapping("/delete/{id}")
    public ApiRes deleteById(@PathVariable Integer id){
        return productService.deleteById(id);
    }
    @GetMapping("/all")
    public ApiRes getAll(){
        return productService.findAll();
    }

    @GetMapping("/byId/{id}")
    public ApiRes getByProductId(@PathVariable Integer id){
        return productService.findAllById(id);
    }

    @GetMapping("/getByPage")
    public ApiRes getByPage(@RequestParam int page, @RequestParam int size){
        return productService.getByPage(page,size);
    }

    @PostMapping("/filter")
    public HttpEntity<?> getByFilter(@RequestBody ReqProductFilter reqProductFilter){
        ApiRes apiRes=productService.filter(reqProductFilter);
        return ResponseEntity.ok(apiRes);
    }

}
