package uz.pdp.b21jpapracticesimple.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import uz.pdp.b21jpapracticesimple.entity.Product;
import uz.pdp.b21jpapracticesimple.enums.CustomFilterType;
import uz.pdp.b21jpapracticesimple.payload.ApiRes;
import uz.pdp.b21jpapracticesimple.payload.ReqProduct;
import uz.pdp.b21jpapracticesimple.payload.ReqProductFilter;
import uz.pdp.b21jpapracticesimple.repository.ProductRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;

    public ApiRes save(Product product){
        productRepository.save(product);
        return new ApiRes("saved",true);
    }

    public ApiRes getById(Integer id) {
        Optional<Product> byId = productRepository.findById(id);
        return new ApiRes("bor",true,byId);
    }

    public ApiRes deleteById(Integer id) {
        productRepository.remove(id);
        return new ApiRes("successfully deleted ",true);
    }

    public ApiRes findAll() {
        List<Product> all = productRepository.findAll();
        return new ApiRes("All Products",true,all);
    }

    public ApiRes findAllById(Integer id) {
        Optional<Product> byId = productRepository.findById(id);
        if (byId.isPresent()){
            Product product = byId.get();
            return new ApiRes("Product By id",true,product);
        }
        return new ApiRes("Such ProductId doesn't exist",false);
    }

    public ApiRes getByPage(int page, int size) {
        Page<Product> all = productRepository.findAll(PageRequest.of(page, size, Sort.by("id").descending()));
        return new ApiRes("Product By id",true,all);
    }

    public ApiRes filter(ReqProductFilter reqProductFilter) {
        List<Product> products=new ArrayList<>();
        if (reqProductFilter.getFilterType().equals(CustomFilterType.CONTAINS)){
            if (reqProductFilter.isPriceDescending()){
                products=productRepository.findAllByNameContainingOrderByPriceDesc(reqProductFilter.getName());

            }else {
                products=productRepository.findAllByNameContainingOrderByPrice(reqProductFilter.getName());
            }
        }else if (reqProductFilter.getFilterType().equals(CustomFilterType.STARTS_WITH)){

            if (reqProductFilter.isPriceDescending()){
                products=productRepository.findAllByNameStartsWithOrderByPriceDesc(reqProductFilter.getName());

            }else {
                products=productRepository.findAllByNameStartsWithOrderByPrice(reqProductFilter.getName());
            }
        }else if (reqProductFilter.getFilterType().equals(CustomFilterType.ENDS_WITH)){

            if (reqProductFilter.isPriceDescending()){
                products=productRepository.findAllByNameEndingWithOrderByPriceDesc(reqProductFilter.getName());

            }else {
                products=productRepository.findAllByNameEndingWithOrderByPrice(reqProductFilter.getName());
            }
        }
        return new ApiRes("sorted objects",true,products);

    }


}
