package uz.pdp.b21jpapracticesimple.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.b21jpapracticesimple.entity.Input;
import uz.pdp.b21jpapracticesimple.entity.InputProduct;
import uz.pdp.b21jpapracticesimple.payload.ApiRes;
import uz.pdp.b21jpapracticesimple.payload.ReqInput;
import uz.pdp.b21jpapracticesimple.payload.ReqInputProduct;
import uz.pdp.b21jpapracticesimple.repository.InputProductRepository;
import uz.pdp.b21jpapracticesimple.repository.InputRepository;
import uz.pdp.b21jpapracticesimple.repository.ProductRepository;
import uz.pdp.b21jpapracticesimple.repository.WarehouseRepository;

import javax.persistence.criteria.CriteriaBuilder;
import java.sql.Timestamp;
import java.util.Date;

@Service
public class InputService {
    @Autowired
    WarehouseRepository warehouseRepository;

    @Autowired
    InputRepository inputRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    InputProductRepository inputProductRepository;

    @Transactional
    public ApiRes save(ReqInput reqInput) {
        Input input = new Input();
        input.setDate(new Timestamp(new Date().getTime()));
        input.setWarehouse(warehouseRepository.findById(reqInput.getWarehouse_id()).orElse(null));
        inputRepository.saveAndFlush(input);
        for (ReqInputProduct reqInputProduct : reqInput.getReqInputProductList()) {
            InputProduct inputProduct = new InputProduct();
            inputProduct.setInput(input);
            inputProduct.setProduct(productRepository.findById(reqInputProduct.getProduct_id()).orElse(null));
            inputProduct.setAmount(reqInputProduct.getAmount());
            inputProductRepository.save(inputProduct);
        }
        return new ApiRes("Saved", true);
    }
}
