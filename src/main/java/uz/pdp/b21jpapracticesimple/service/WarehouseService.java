package uz.pdp.b21jpapracticesimple.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.b21jpapracticesimple.entity.Warehouse;
import uz.pdp.b21jpapracticesimple.payload.ApiRes;
import uz.pdp.b21jpapracticesimple.repository.WarehouseRepository;

import java.util.Optional;

@Service
public class WarehouseService {

    @Autowired
    WarehouseRepository warehouseRepository;


    public ApiRes save(Warehouse warehouse) {
        Warehouse save = warehouseRepository.save(warehouse);
        return new ApiRes("Saved", true);
    }

    public ApiRes getAll() {
        return new ApiRes("Ok", true, warehouseRepository.findAll());
    }

    public ApiRes getById(Integer id) {
        Optional<Warehouse> byId = warehouseRepository.findById(id);
        return byId.map(warehouse -> new ApiRes("Ok", true, warehouse)).orElseGet(() -> new ApiRes("Error", false));
//        warehouseRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Error"))
    }
}
