package uz.pdp.b21jpapracticesimple;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class B21JpaPracticeSimpleApplication {

    public static void main(String[] args) {
        SpringApplication.run(B21JpaPracticeSimpleApplication.class, args);
    }

}
