package uz.pdp.b21jpapracticesimple.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.b21jpapracticesimple.entity.Input;
import uz.pdp.b21jpapracticesimple.entity.Product;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface ProductRepository extends JpaRepository<Product,Integer> {

    List<Product> findAllByNameContainingOrderByPrice(String name);
    List<Product> findAllByNameContainingOrderByPriceDesc(String name);

    List<Product> findAllByNameStartsWithOrderByPrice(String name);
    List<Product> findAllByNameStartsWithOrderByPriceDesc(String name);

    List<Product> findAllByNameEndingWithOrderByPrice(String name);
    List<Product> findAllByNameEndingWithOrderByPriceDesc(String name);

    @Transactional
    @Modifying
    @Query(value = "delete from product where id=:id",nativeQuery = true)
    void remove(@Param(value = "id") Integer id);
}
