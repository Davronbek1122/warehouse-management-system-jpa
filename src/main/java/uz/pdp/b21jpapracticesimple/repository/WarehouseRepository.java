package uz.pdp.b21jpapracticesimple.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.b21jpapracticesimple.entity.Product;
import uz.pdp.b21jpapracticesimple.entity.Warehouse;

public interface WarehouseRepository extends JpaRepository<Warehouse,Integer> {
}
