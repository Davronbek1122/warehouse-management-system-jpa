package uz.pdp.b21jpapracticesimple.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.b21jpapracticesimple.entity.Input;

public interface InputRepository extends JpaRepository<Input,Integer> {
}
