package uz.pdp.b21jpapracticesimple.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.b21jpapracticesimple.entity.Input;
import uz.pdp.b21jpapracticesimple.entity.InputProduct;

public interface InputProductRepository extends JpaRepository<InputProduct,Integer> {
}
