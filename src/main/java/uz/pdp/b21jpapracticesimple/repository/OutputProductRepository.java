package uz.pdp.b21jpapracticesimple.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.b21jpapracticesimple.entity.InputProduct;
import uz.pdp.b21jpapracticesimple.entity.OutputProduct;

public interface OutputProductRepository extends JpaRepository<OutputProduct,Integer> {
}
